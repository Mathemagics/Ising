import numpy as NP
import scipy as SP
import scipy.constants as Const
import math
import matplotlib.pyplot as Plt
from matplotlib import animation

def generateSigLattice(maximum=(16)**2): #Pure
    def randomSig(nothing=0): #Pure
        a = NP.random.choice([1,-1])
        return a

    def sigList(): #Pure
        a = map(randomSig, range(0,maximum))
        return a

    a = NP.array(sigList()).reshape(int(math.sqrt(maximum)), int(math.sqrt(maximum))) #Pure
    return a

def randomSigFlip(): #IMPURE SO IMPURE
    x,y = NP.shape(mutatedLattice)
    randomX = NP.random.choice(range(0,x))
    randomY = NP.random.choice(range(0,y))
    mutatedLattice[randomX][randomY] = mutatedLattice[randomX][randomY]*(-1)
    #print (randomX,randomY)
    return mutatedLattice

def deltaEnergy(staticLattice, mutatedLattice):
    return hamiltonian(staticLattice) - hamiltonian(mutatedLattice)

def hamiltonian(lattice):
    x,y = NP.shape(lattice)
    accumulator=0
    for i in range(x-1):
        for j in range(y-1):
            neighborList=atomicNeighbors(lattice,i,j)
            productSum = sum(map((lambda x: lattice[i,j] * x), neighborList))
            accumulator += productSum
    return accumulator

def boltzmanDistribution(staticLattice, mutatedLattice): #pure
    boltzmanConst= Const.k
    temperature = 1 #Kelvin
    deltaE = deltaEnergy(staticLattice, mutatedLattice)
    boltzmanP = math.exp(-deltaE/(boltzmanConst*temperature))
    print("Boltzman P", boltzmanP)
    return boltzmanP
    
def boltzmanBoolean(staticLattice, mutatedLattice): #pure
                randomPercent = NP.random.random()
                if randomPercent >= boltzmanDistribution(staticLattice, mutatedLattice):
                    print("Boltzman True:", randomPercent)
                    return True
                else:
                    print("Boltzman False", randomPercent)
                    return False
            
def checkEnergy(staticLattice, mutatedLattice):     #pure            
            if deltaEnergy(staticLattice, mutatedLattice) < 0:
                print("return unconditionally")
                return mutatedLattice
            
            if (deltaEnergy(staticLattice, mutatedLattice) > 0) and boltzmanBoolean(staticLattice, mutatedLattice):
                print("accept with condition")
                return mutatedLattice
                
            else:
                print("Rejection")
                return staticLattice 
            
def atomicNeighbors(lattice, y, x): #there must be a better way! The boundaries must be respected! #pure
    size = lattice.size
    maxCol = int(math.sqrt(size))-1
    maxRow = maxCol
    
    neighbors=[]
    if (y==0 and x==0): #topLeft
        neighbors.append(lattice[y][x+1])
        neighbors.append(lattice[y+1][x])
        
    elif (y==maxCol and x==0): #bottomLeft
        neighbors.append(lattice[y-1][x])
        neighbors.append(lattice[y][x+1])
    
    elif (y==0 and x==maxCol): #topRight
        neighbors.append(lattice[y+1][x])
        neighbors.append(lattice[y][x-1])
        
    elif (y==maxRow and x==maxCol): #bottomRight
        neighbors.append(lattice[y-1][x])
        neighbors.append(lattice[y][x-1])
        
    elif ((y!=0 or y!=maxRow) and x==0): #Left inner column
        neighbors.append(lattice[y+1][x])
        neighbors.append(lattice[y-1][x])
        neighbors.append(lattice[y][x+1])
        
    elif (y==0 and (x!=0 or x!=maxCol)): #top inner row
        neighbors.append(lattice[y+1][x])
        neighbors.append(lattice[y][x+1])
        neighbors.append(lattice[y][x-1])    
        
    elif (x==maxCol and (y!=0 or y!=maxRow)): #right inner column
        neighbors.append(lattice[y+1][x])
        neighbors.append(lattice[y-1][x])
        neighbors.append(lattice[y][x-1])
       
    elif (y==maxRow and (x!=0 or x!=maxCol)): #left row column
        neighbors.append(lattice[y+1][x]) 
        neighbors.append(lattice[y-1][x])
        neighbors.append(lattice[y][x-1])
    
    else: #center
        neighbors.append(lattice[y+1][x])
        neighbors.append(lattice[y-1][x])
        neighbors.append(lattice[y][x-1])
        neighbors.append(lattice[y][x+1])
    return neighbors

#lattice = NP.arange(100).reshape(10, 10)



def main():
    global staticLattice
    global mutatedLattice
    staticLattice = generateSigLattice() 
    mutatedLattice = NP.copy(staticLattice) #IMPURE
    initLattice=NP.copy(staticLattice)
    fig = Plt.figure()
    im = Plt.imshow(initLattice, cmap="hot", interpolation="nearest")

    def ising(staticLattice, mutatedLattice): #IMPURE
        randomSigFlip() #very impure
        staticLattice = NP.copy(checkEnergy(staticLattice, mutatedLattice)) #IMPURE
        return staticLattice

    def generateFrames(epochs=100):
        frames=[]
        for i in range(epochs):
            frames.append(ising(staticLattice, mutatedLattice))
        return frames
    
    def update(data):
        im.set_data(retFrame())
        return im

    frames = generateFrames()
    for i in frames:
        print(i)
        fig.clear()
        im = Plt.imshow(i, cmap="hot", interpolation="nearest")
        Plt.show()

main()
